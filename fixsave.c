/*
 * MIT License
 *
 * Copyright (c) 2022 Jaime Ita Passos
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <stdarg.h>
#include <string.h>

static void Error(const char *format, ...)
{
    char buffer[4096];

    va_list args;
    va_start(args, format);
    vsnprintf(buffer, sizeof buffer, format, args);
    va_end(args);

    fprintf(stderr, "Error: %s\n", buffer);
    exit(EXIT_FAILURE);
}

#define NEW_SKIN_SAVES_ID 0x7FFF
#define EMERALDS_OFFSET 0x165
#define LUA_BANKS_MARKER 0xB7
#define SAVEGAME_END_MARKER 0x1D

#define VERSION_STRING_LENGTH 16
#define SKIN_NAME_LENGTH 16
#define MOD_NAME_LENGTH 64

#define NUM_GAME_SKINS 6
#define NUM_LUA_BANKS 16

struct SaveData {
    uint8_t version[VERSION_STRING_LENGTH];

    uint8_t skin, botSkin;
    int32_t score, lives, continues;
    uint16_t emeralds;
    uint8_t numGameOvers;
    int16_t gameMapID;

    char modname[MOD_NAME_LENGTH];
    int32_t luaBanks[NUM_LUA_BANKS];
};

#define DEF_READ_FUNCTION(funcName, type) \
static type funcName(FILE *f) \
{ \
    type out; \
 \
    if (fread(&out, sizeof out, 1, f) != 1) { \
        if (feof(f)) \
            Error("Unexpected end of file"); \
        else \
            Error("Error reading file"); \
    } \
 \
    return out; \
}

DEF_READ_FUNCTION(ReadUint8, uint8_t);
DEF_READ_FUNCTION(ReadUint16, uint16_t);
DEF_READ_FUNCTION(ReadUint32, uint32_t);
DEF_READ_FUNCTION(ReadInt8, int8_t);
DEF_READ_FUNCTION(ReadInt16, int16_t);
DEF_READ_FUNCTION(ReadInt32, int32_t);

static void ReadBytes(FILE *f, uint8_t *buffer, size_t length)
{
    if (fread(buffer, sizeof (*buffer), length, f) != length) {
        if (feof(f))
            Error("Unexpected end of file");
        else
            Error("Error reading file");
    }
}

static void ReadString(FILE *f, char *buffer, size_t size)
{
    size_t i = 0;

    while (i < size && (buffer[i] = ReadUint8(f)) != '\0')
        i++;

    buffer[i] = '\0';
}

static void ReadMisc(FILE *f, struct SaveData *savedata)
{
    savedata->gameMapID = ReadInt16(f);
    savedata->emeralds = ReadUint16(f) - EMERALDS_OFFSET;

    ReadString(f, savedata->modname, sizeof savedata->modname);
}

static int16_t GuessSkinFromName(const char *name)
{
    const char *skinNames[NUM_GAME_SKINS] = {
        "sonic",
        "tails",
        "knuckles",
        "amy",
        "fang",
        "metalsonic"
    };

    for (int16_t i = 0; i < NUM_GAME_SKINS; i++) {
        if (!strcmp(name, skinNames[i]))
            return i;
    }

    return -1;
}

static void ReadPlayers(FILE *f, struct SaveData *savedata)
{
    int16_t skin = ReadInt16(f);

    if (skin != NEW_SKIN_SAVES_ID)
        Error("Not a 2.2.10 savefile");
    else {
        char playerSkinName[SKIN_NAME_LENGTH + 1];
        ReadString(f, playerSkinName, sizeof playerSkinName);

        if (playerSkinName[0] != '\0') {
            int16_t skinID = GuessSkinFromName(playerSkinName);

            if (skinID == -1)
                Error("Unknown skin name \"%s\"", playerSkinName);

            savedata->skin = skinID;
        } else
            Error("Invalid skin name");

        char botSkinName[SKIN_NAME_LENGTH + 1];
        ReadString(f, botSkinName, sizeof botSkinName);

        if (botSkinName[0] != '\0') {
            int16_t skinID = GuessSkinFromName(botSkinName);

            if (skinID == -1)
                Error("Unknown skin name \"%s\"", botSkinName);

            savedata->botSkin = skinID + 1;
        }
    }

    savedata->numGameOvers = ReadUint8(f);
    savedata->lives = ReadInt8(f);
    savedata->score = ReadUint32(f);
    savedata->continues = ReadInt32(f);
}

static void ReadLuaBanks(FILE *f, struct SaveData *savedata)
{
    switch (ReadUint8(f)) {
        case LUA_BANKS_MARKER:
            uint8_t used = ReadUint8(f);
            if (used > NUM_LUA_BANKS)
                Error("Corrupted save file");
            for (uint8_t i = 0; i < used; i++)
                savedata->luaBanks[i] = ReadInt32(f);
            if (ReadUint8(f) != SAVEGAME_END_MARKER)
                Error("Corrupted save file");
            // fall through
        case SAVEGAME_END_MARKER:
            break;
        default:
            Error("Corrupted save file");
    }
}

static void ReadSave(FILE *f, struct SaveData *savedata)
{
    ReadBytes(f, savedata->version, VERSION_STRING_LENGTH);

    ReadMisc(f, savedata);
    ReadPlayers(f, savedata);
    ReadLuaBanks(f, savedata);
}

#define DEF_WRITE_FUNCTION(funcName, type) \
static void funcName(FILE *f, type in) \
{ \
    if (fwrite(&in, sizeof in, 1, f) != 1) \
        Error("Error writing file"); \
}

DEF_WRITE_FUNCTION(WriteUint8, uint8_t);
DEF_WRITE_FUNCTION(WriteUint16, uint16_t);
DEF_WRITE_FUNCTION(WriteUint32, uint32_t);
DEF_WRITE_FUNCTION(WriteInt8, int8_t);
DEF_WRITE_FUNCTION(WriteInt16, int16_t);
DEF_WRITE_FUNCTION(WriteInt32, int32_t);

static void WriteBytes(FILE *f, uint8_t *buffer, size_t length)
{
    if (fwrite(buffer, sizeof (*buffer), length, f) != length)
        Error("Error writing file");
}

static void WriteString(FILE *f, char *buffer, size_t size)
{
    size_t i = 0;

    for (; i < size && buffer[i] != '\0'; i++)
        WriteUint8(f, buffer[i]);

    if (i < size)
        WriteUint8(f, '\0');
}

static void WriteMisc(FILE *f, struct SaveData *savedata)
{
    WriteInt16(f, savedata->gameMapID);
    WriteUint16(f, savedata->emeralds + EMERALDS_OFFSET);
    WriteString(f, savedata->modname, sizeof savedata->modname);
}

static void WritePlayers(FILE *f, struct SaveData *savedata)
{
    WriteUint16(f, savedata->skin + (savedata->botSkin << 5));
    WriteUint8(f, savedata->numGameOvers);
    WriteInt8(f, savedata->lives);
    WriteUint32(f, savedata->score);
    WriteInt32(f, savedata->continues);
}

static void WriteLuaBanks(FILE *f, struct SaveData *savedata)
{
    uint8_t used = NUM_LUA_BANKS;
    while (used && !savedata->luaBanks[used - 1])
        used--;

    if (used) {
        WriteUint8(f, LUA_BANKS_MARKER);
        WriteUint8(f, used);

        for (uint8_t i = 0; i < used; i++)
            WriteInt32(f, savedata->luaBanks[i]);
    }
}

static void WriteSave(FILE *f, struct SaveData *savedata)
{
    WriteBytes(f, savedata->version, VERSION_STRING_LENGTH);

    WriteMisc(f, savedata);
    WritePlayers(f, savedata);
    WriteLuaBanks(f, savedata);

    WriteUint8(f, SAVEGAME_END_MARKER);
}

int main(int argc, char **argv)
{
    if (argc < 3) {
        fprintf(stderr, "usage: %s <input> <output>\n", argv[0]);
        exit(EXIT_FAILURE);
    }

    struct SaveData savedata = { 0 };

    FILE *f = fopen(argv[1], "rb");
    if (!f)
        Error("Couldn't open %s", argv[1]);

    ReadSave(f, &savedata);
    fclose(f);

    f = fopen(argv[2], "wb");
    if (!f)
        Error("Couldn't open %s", argv[2]);

    WriteSave(f, &savedata);
    fclose(f);

    return EXIT_SUCCESS;
}
